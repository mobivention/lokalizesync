try:
    from configparser import ConfigParser
except ImportError:
    from ConfigParser import ConfigParser

import argparse
from .client import LokaliseError, LokaliseClient
import os
from time import sleep
import click
import zipfile

class LokaliseConfigError(LokaliseError):
    pass


class LokaliseConfig:

    def __repr__(self):
        return str(vars(self))

    def _verify_section(self, name):
        if not self.config.has_section(name):
            raise LokaliseConfigError("Config Section missing: {name}".format(name=name))
        return self.config.options(name)

    def _option_value(self, section, name, default=None):
        if not self.config.has_option(section, name):
            if default:
                return  default
            else:
                raise LokaliseConfigError("Config Option missing: {name}".format(name=name))
        return self.config.get(section, name)

    def __init__(self, filepath, api_key):
        self.api_key = api_key
        if not os.path.isfile(filepath):
            raise LokaliseConfigError("Config missing")
        self.config = ConfigParser(defaults={"file_encoding": "utf-8", "input_file_base_path": "."})
        self.config.read([filepath])

        section = "Lokalise"
        # Check for old api keys in config, which we don't want to have anymore
        if self.config.has_section(section):
            api_key = self.config.has_option(section, "api_key")
            if api_key:
                raise LokaliseConfigError("Please remove the API key from your config from now on")

        section = "Project"
        self._verify_section(section)
        self.name = self._option_value(section, "name")
        self.file_encoding = self._option_value(section, "file_encoding")
        self.input_file_base_path = self._option_value(section, "input_file_base_path")
        self.output_file_base_path = self._option_value(section, "output_file_base_path")
        self.base_lang = self._option_value(section, "base_lang")

        section = "Input"
        file_keys = self._verify_section(section)
        for k in self.config.defaults().keys():
            file_keys.remove(k)
        self.files = [self.config.get(section, k) for k in file_keys]

        section = "Output"
        self.file_type = self._option_value(section, "type")
        self.bundle_structure = self._option_value(section, "structure")
        self.export_empty = self._option_value(section, "export_empty", default="empty")

def get_api_key():
    if os.path.exists(".lokalise/api_key"):
        with open('.lokalise/api_key') as f:
            return f.read()
    else:
        key = click.prompt('Please enter your API Key. Please do not commit the file .lokalise/api_key to your repository')
        try:
            os.mkdir(".lokalise")
        except:
            pass
        with open('.lokalise/api_key', 'w') as f:
            f.write(key)
        return(key)


@click.group()
def cli():
    pass


@cli.command()
@click.option('--force', default=False, type=bool, help="Overwrite existing texts when uploading strings")
def upload(force):
    try:
        key = get_api_key()
        config = LokaliseConfig("lokalise.ini", key)

        client = LokaliseClient(config.api_key)
        for idx, filepath in enumerate(config.files):
            if idx > 0:
                print("Wait 30 seconds because of Lokalise throttling")
                sleep(30)
            path = os.path.join(config.input_file_base_path, filepath)

            project = client.project_with_name(config.name)
            if not project:
                raise LokaliseConfigError("Project with the given name does not exist")

            client.import_data(project, path, config.base_lang)

            print("Imported file {filename}".format(filename=filepath))
    except LokaliseError as e:
        print(dir(e))
        print("Error: %s" % e.message)


@cli.command()
def download():
    key = get_api_key()
    config = LokaliseConfig("lokalise.ini", key)

    client = LokaliseClient(config.api_key)

    project = client.project_with_name(config.name)
    path = config.output_file_base_path
    if not project:
        raise LokaliseConfigError("Project with the given name does not exist")

    client.export_data(project,
                       path,
                       config.file_type,
                       config.bundle_structure,
                       export_empty=config.export_empty)


if __name__ == '__main__':
    cli()