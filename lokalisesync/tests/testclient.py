import lokalisesync
from lokalisesync.client import LokaliseClient, LokaliseProject
import unittest
from time import sleep
import os

#account: sbusch@mailinator.com - muhkuh456
api_key = "bcbc0881d04b0d297deef835d0b859aa4fbd9e94"


class ProjectTestCase(unittest.TestCase):

    def setUp(self):
        self.client = LokaliseClient(api_key)

    def testProjectList(self):

        p = self.client.list_projects()
        self.assertEqual(len(p), 1)
        self.assertIsInstance(p[0], LokaliseProject)
        self.assertEqual(p[0].name, "ExistingTest")

    def testProjectAddRemove(self):
        self.client.add_project("test", "Nur ein test", "en")
        sleep(5)
        p = self.client.list_projects()
        self.assertEqual(len(p), 2)

        new_one = None
        for project in p:
            self.assertIsInstance(project, LokaliseProject)
            if project.name == "test":
                new_one = project

        self.assertIsNotNone(new_one)
        sleep(5)
        self.client.remove_project(new_one)
        sleep(5)
        p = self.client.list_projects()

        self.assertEqual(len(p), 1)
        self.assertIsInstance(p[0], LokaliseProject)
        self.assertEqual(p[0].name, "ExistingTest")
        sleep(5)

    def testImport(self):
        p = self.client.list_projects()
        sleep(5)
        project = p[0]
        self.client.import_data(project, os.path.join(os.path.dirname(os.path.realpath(__file__)), 'Localizable.strings'), 'de')
        sleep(30)