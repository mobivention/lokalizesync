from unittest import TestCase
from lokalisesync import sync
import requests_mock
import os
from click.testing import CliRunner
import shutil

mock_projects = """{
	"projects":
		[
			{
				"id":"2178052454e5eecbe36c68.09719217",
				"name":"Tippspiel",
				"desc":"Lokalise sample project.",
				"created":"2015-02-05 23:14:05",
				"owner":"0"
			},
			{
				"id":"1412623154e5ef27b06bb2.34713953",
				"name":"Demo Project",
				"desc":"",
				"created":"2015-02-19 16:11:51",
				"owner":"1"
			}
		],
	"response":
		{
			"status":"success",
			"code":"200",
			"message":"OK"
		}
}"""

mock_export = """
    {
        "bundle":
            {
                "file":"\/export\/Tippspiel-Localizable.zip"
            },
        "response":
            {
                "status":"success",
                "code":"200",
                "message":"OK"
            }
    }

"""

base = os.path.dirname(os.path.realpath(__file__))

class ConfigTestCase(TestCase):

    def testConfig(self):
        self.assertRaises(sync.LokaliseConfigError, sync.LokaliseConfig, os.path.join(base, 'tes1t.ini'), "bla")
        self.assertRaises(sync.LokaliseConfigError, sync.LokaliseConfig, os.path.join(base, 'configs/incomplete.ini'), "bla")
        self.assertRaises(sync.LokaliseConfigError, sync.LokaliseConfig, os.path.join(base, 'configs/testold.ini'), "bla")
        self.assertRaises(TypeError, sync.LokaliseConfig, os.path.join(base, 'configs/test.ini'))

        sync.LokaliseConfig(os.path.join(base, 'configs/test.ini'), "bla")
        sync.LokaliseConfig(os.path.join(base, 'skeletons/ios/lokalise.ini'), "bla")


class SyncTestCase(TestCase):

    def testSync(self):
        os.chdir(os.path.join(base, "skeletons/ios"))
        try:
            os.remove(".lokalise/api_key")
        except:
            pass
        with requests_mock.mock() as m:
            m.get("https://lokalise.co/api/project/list?api_token=blablub", text=mock_projects)
            m.post("https://lokalise.co/api/project/import", text='{"message": "success"}')
            runner = CliRunner()
            result = runner.invoke(sync.upload, input="blablub")
            print(result)
            self.assertEqual(result.exit_code, 0)

    def testDownload(self):
        os.chdir(os.path.join(base, "skeletons/ios"))
        try:
            shutil.rmtree('Tippspiel/de.lproj')
        except:
            pass
        try:
            os.remove(".lokalise/api_key")
        except:
            pass
        self.assertFalse(os.path.exists('Tippspiel/de.lproj'))
        with requests_mock.mock() as m:
            with open('Tippspiel-Localizable.zip', 'rb') as f:
                m.get("https://lokalise.co/api/project/list?api_token=blablub", text=mock_projects)
                m.post("https://lokalise.co/api/project/export", text=mock_export)
                m.get("https://lokalise.co/export/Tippspiel-Localizable.zip", body=f)

                runner = CliRunner()
                result = runner.invoke(sync.download, input="blablub")
                print(result)

                self.assertEqual(result.exit_code, 0)
                self.assertTrue(os.path.exists('Tippspiel/de.lproj/Localizable.strings'))