import requests
import chardet
import six

import codecs
import zipfile
import os

try:
    from urllib.parse import urljoin
except ImportError:
    from urlparse import urljoin

class BaseModel:

    fields = []

    @classmethod
    def from_dict(cls, dict):
        o = cls()
        for property, _ in vars(cls).items():
            if property in dict:
                setattr(o, property, dict[property])

        return o

    def to_dict(self):
        return {(k, v) for k, v in vars(self).items()}

    def __repr__(self):
        return "<{c}> [{value}]".format(c=self.__class__.__name__, value=vars(self))


class LokaliseProject(BaseModel):

    id = ""
    name = ""
    desc = ""
    created = ""
    owner = ""


class LokaliseError(Exception):
    pass


class LokaliseClient:

    base_url = "https://lokalise.co/api"

    def __init__(self, api_key):
        self.api_key = api_key

    def _api_request(self, path, method='get', headers=None, args=None, files=None):
        methods = {'get': requests.get, 'post': requests.post, 'put': requests.put, 'delete': requests.delete}

        f = methods[method]

        if not args:
            args = {}
        args["api_token"] = self.api_key

        url = urljoin(self.base_url, path)
        print(url)
        if method == 'get':
            res = f(url, headers=headers, params=args)
        else:
            # res = f("http://requestb.in/14tjhb51", headers=headers, data=args, files=files)
            res = f(url, headers=headers, data=args, files=files)

        if res.status_code != 200:
            raise LokaliseError("Request error")

        return res
    def list_projects(self):

        res = self._api_request("/api/project/list").json()

        return [LokaliseProject.from_dict(d) for d in res["projects"]]

    def project_with_name(self, name):
        projects = self.list_projects()
        print(projects)
        hit = [p for p in projects if p.name == name]
        if len(hit) > 0:
            return hit[0]

        return None

    def add_project(self, name, description, base_lang):
        res = self._api_request("/api/project/add", method='post', args={"name": name, "description": description, "base_lang": base_lang}).json()
        return LokaliseProject.from_dict(res["project"])

    def remove_project(self, project):
        self._api_request("/api/project/remove", method='post', args={"id": project.id})

    def import_data(self, project, filepath, language, encoding='utf-8'):
        raw = open(filepath, mode="rb")
        det = chardet.detect(raw.read())
        if det["confidence"] > 0.9:
            encoding = det["encoding"]
        with codecs.open(filepath, encoding=encoding) as f:
            res = self._api_request("/api/project/import", method='post', args={
                'id': project.id,
                'lang_iso': language,
                'fill_empty': 1
            }, files={
                'file': f
            }).json()
            print(res)

    def export_data(self,
                    project,
                    filepath,
                    type,
                    bundle_structure,
                    export_empty='empty'):
        res = self._api_request("/api/project/export", method='post', args={
            'id': project.id,
            'type': type,
            'bundle_structure': bundle_structure,
            'export_empty': export_empty
        }).json()
        print(res)
        zip_path = res['bundle']['file']
        try:
            zip = self._api_request(zip_path, method='get')

            with open("trans.zip", 'wb') as f:
                for chunk in zip:
                    f.write(chunk)

            zip_ref = zipfile.ZipFile("trans.zip", 'r')
            zip_ref.extractall(filepath)
            zip_ref.close()
        except Exception as e:
            print(e)
        finally:
            try:
                os.remove("trans.zip")
            except:
                pass