LokaliseSync
============

You can use this tool to automatically sync your project strings and translations with Lokalise (https://lokalise.co). Python 2 and 3 compatible

### Features

* String upload
* String download

### Roadmap

* Automatic Tagging
* Please request more :)

Installation
------------
If on Windows: Install Python (2.7+ or 3.4+ should both be fine) from https://www.python.org/downloads/. Open a command line. Run `python` and `pip`. If anything fails, make sure your python-path was added to PATH

Please use the following command to install or upgrade the package:

	pip install -U https://bitbucket.org/mobivention/lokalizesync/get/master.zip
    
If you get permission errors (i.e. you are on Mac OS and not in a virtualenv), please use this one:

	pip install -U --user https://bitbucket.org/mobivention/lokalizesync/get/master.zip
	
Configuration
-------------

Add a file named `lokalise.ini` your Project root directory. It should look like this:
	
	[Project]
	name = Tippspiel
	input_file_base_path = Tippspiel/en.lproj
	output_file_base_path = Tippspiel
	base_lang = en
	
	[Input]
	Info = InfoPlist.strings
	Localizable = Localizable.strings
	Main = Main.strings
	
	[Output]
    type = strings
    structure = %LANG_ISO%.lproj/Localizable.%FORMAT%
    export_empty = base
	
* You will be prompted for your api key once. Please do not commit the file `.lokalise/api_key` to your repository	
    * Please remove the section "Lokalise" and key "api_key" from your config if still present
    * You can find your api key on the Lokalise Website at "Account settings/API tokens". It is NOT your project id
* More (optional) keys:
	* `file_encoding`
* 'project' needs to be the project's name in Lokalise
* You can find your API key in your Lokalise settings
* [Input] is a list of Key-Value pairs of arbitrary length
    * The key names in [Input] are arbitrary and you can name them any way you want. The might be used for tagging purposes later on
    * The values need to be the file names relative to the `input_file_base_path`
* Structure is to be used according to Lokalise:
    * You can use %LANG_ISO%, %LANG_NAME%, %FORMAT%, %PROJECT_NAME% placeholders.
* Supported values for `type` can be found here: https://docs.lokalise.co/category/Vjccih4yFX-supported-file-formats
* `export_empty` values are: 'empty' (default), 'base' and 'skip'

Usage
-----

Run the command `lokalisesync upload` or `lokalisesync download` from your project's root directory.