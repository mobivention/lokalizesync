# -*- coding: UTF-8 -*-
from setuptools import setup

def readme():
      with open('README.md') as f:
            return f.read()

setup(name='lokalisesync',
      version='0.2',
      long_description=readme(),
      description='Sync your localisations with lokalise',
      url='https://bitbucket.org/mobivention/lokalizesync',
      author='Sören Busch',
      author_email='sbusch@mobivention.com',
      license='MIT',
      packages=['lokalisesync'],
      include_package_data=True,
      entry_points='''
        [console_scripts]
        lokalisesync=lokalisesync.sync:cli
    ''',
      zip_safe=False,
      install_requires=[
            'requests',
            'chardet',
            'six',
            'click'
      ])